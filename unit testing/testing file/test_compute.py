import unittest
from classfile import compute

class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(compute.o.myfunc(3),1.92)
        self.assertEqual(compute.o.myfunc(4),2.72)
        self.assertEqual(compute.o.myfunc(1),0.50)


if __name__ == '__main__':
    unittest.main()
