import unittest
import classfile
from classfile.add import myclass
p= myclass(5,10)
class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(p.z,15)

if __name__ == '__main__':
    unittest.main()
