import unittest
from classfile import factorial

class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(factorial.o.myfunc(2),2)
        self.assertEqual(factorial.o.myfunc(5),120)
        self.assertEqual(factorial.o.myfunc(3),6)

if __name__ == '__main__':
    unittest.main()
