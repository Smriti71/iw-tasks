import unittest
from classfile import circlearea

class MyTestCase(unittest.TestCase):
    def test_calculate(self):
        self.assertEqual(circlearea.o.calculate(3),28.26)
        self.assertEqual(circlearea.o.calculate(2),12.56)
        self.assertEqual(circlearea.o.calculate(1),3.14)

if __name__ == '__main__':
    unittest.main()
