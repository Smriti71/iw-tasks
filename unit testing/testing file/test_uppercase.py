import unittest
from classfile import uppercase

class MyTestCase(unittest.TestCase):
    def test_something(self):
        s=uppercase.myclass('smriti')
        self.assertEqual(s.upper(),'SMRITI')


if __name__ == '__main__':
    unittest.main()
