
// $(document).ready(function() {
//   // Initialize form validation on the registration form.
//   // It has the name attribute "registration"
//   $("form[name='myform']").validate({
//     // Specify validation rules
//     rules: {
//       // The key name on the left side is the name attribute
//       // of an input field. Validation rules are defined
//       // on the right side
//       fullname: {
//         required:true,
//         maxlength:15
//       },
//       textarea:{
//         required:true,
//         maxlength:300,
//         minlength:20
//       },
//       phone:{
//         required:true,
//         number:true,
//         maxlength:10,
//         minlength:10
//       },
//       email: {
//         required: true,
//         // Specify that email should be validated
//         // by the built-in "email" rule
//         email: true
//       },
//     },
//     // Specify validation error messages
//     messages: {
//       fullname: {
//         required:"Please enter your fullname",
//         maxlength:"Your name should be at most 15 characters long"
//       },
//       textarea:{
//         required:"Please share your motivation",
//         maxlength:"It should be atleast 20 characters long"
//       },
//       phone:{
//         required:"Please enter your phone no",
//         maxlength:"It should be exact 10 characters long",
//         minlength:"It should be exact 10 characters long",
//       },
//       email: "Please enter a valid email address"
//     },
    
//     // Make sure the form is submitted to the destination defined
//     // in the "action" attribute of the form when valid
//     submitHandler: function(form) {
//       form.submit();
//     }
//   });
// });

// $(document).ready(function(){
//   console.log("haha");
//   var fullname = $('#full_name').val();
//   var tel = $('#phone_name').val();
//   // var cvfile = $('#cv').val();
//   var frmmotivation = $('#frmmotivation').val();
//   var email = $('#email_name').val();

//   var email_regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
//   var name_regex = new RegExp(/^[a-zA-Z]+$/);
//   // /^([a-zA-Z]+)$/;
//   var phone_regex = /^((\+*)[9]77-*)*[9]{1}[0-9]{9}$/;

//   $("#full_name").focusout( function(){
//     console.log("full");
//     if (name_regex.test(fullname)) {
//       console.log("whatdown");
//       $("#div1").hide();

//       // return false;
//   }
//   else{
//     console.log("what");
//     $("#div1").html(" For your name please use alphabets only "); // This Segment Displays The Validation Rule For Name
//     $("#div1").css("color", "red");
//     // $("#full_name").focus();
//     $("#div1").show();
//   }
//   });

$(function(){

  $("#full_name").blur( function(){
    check_name();
  });

  $("#phone_name").blur( function(){
    check_phone();
  });

  $("#frmmotivation").blur( function(){
    check_motivation();
  });

  $("#email_name").blur(function(){
    check_email();
  });

  });

  function check_name(){
    var fullname = $('#full_name').val();
    var name_regex = /^([a-zA-Z]+)$/;
    if (fullname.match(name_regex)) {
        $("#div1").hide();
        // $("#full_name").focus();
        // return false;
    }
    else{
      $("#div1").text(" For your name please use alphabets only "); // This Segment Displays The Validation Rule For Name
      $("#div1").css("color", "red");
      $("#div1").show();
    }
  }

  function check_email(){
    var email = $('#email_name').val();
    var email_regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if (!email.match(email_regex) || email.length == 0) {
        $('#div2').text(" Please enter a valid email address "); // This Segment Displays The Validation Rule For Email
        $("#div2").css("color", "red");
        $("#email_name").focus();
        return false;
    }
    else {
        $("#div2").hide();
    }
  }

  function check_phone(){
    var tel = $('#phone_name').val();
    var phone_regex = /^((\+*)[9]77-*)*[9]{1}[0-9]{9}$/;
    if (!tel.match(phone_regex) || tel.length == 0) {
        $("#div3").text("This field should not be empty");
        $("#div3").css("color", "red");
        $("#phone_name").focus();
        return false;
        
    }

    else{
        $("#div3").hide();
    }
  }

  function check_motivation(){
    var frmmotivation = $('#frmmotivation').val();
    if (frmmotivation.length == 0) {
        $('#div4').html(" For Address please use numbers and letters "); // This Segment Displays The Validation Rule For Address
        $("#div4").css("color", "red");
        $("#frmmotivation").focus();
        return false;
    }
    else{
        $("#div4").hide();
    }
  }

  $("#sbtn").click(function (e) {
       // Initializing Variables With Form Element Values
     
      e.preventDefault();

      
      // Initializing Variables With Regular Expressions

      console.log("inside jquery");
      check_name();
      check_email();
      check_phone();
      check_motivation();
      
      
  });


// $(function(){
//   var full_name = $('#full_name').val();
//   var name_regex = new RegExp(/^[a-zA-Z]+$/);
//   $("#full_name").focusout( function(){
//     console.log("full");
//     if (name_regex.test(full_name)) {
//       console.log("whatdown");
//       $("#div1").hide();
//       return false
//       // return false;
//   }
//   else{
//     console.log("what");
//     $("#div1").html(" For your name please use alphabets only "); // This Segment Displays The Validation Rule For Name
//     $("#div1").css("color", "red");
//     // $("#full_name").focus();
//     $("#div1").show();
//   }
//   });
// });
