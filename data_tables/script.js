// $(document).ready( function () {
//     var t=$('#example').DataTable();
//         $.ajax({
//           type:"get",
//           url:"http://jsonplaceholder.typicode.com/users",
//           success: function(data){
//               var json = data;
//               var tr;
//               for (var i = 0; i < json.length; i++) {
//                 tr = $('<tr/>');
//                 tr.append("<td>" + json[i].id + "</td>");
//                 tr.append("<td>" + json[i].name + "</td>");
//                 tr.append("<td>" + json[i].email + "</td>");
//                 tr.append("<td>" + json[i].username + "</td>");
//                 tr.append("<td>" + json[i].phone + "</td>");
//                 tr.append("<td>" + json[i].website + "</td>");
//                 $('#example').append(tr);
//             }
//         }
//       });
// } );


// $(document).ready(function() {
//     $.ajax({
//         type:"get",
//         url:"http://jsonplaceholder.typicode.com/users",
//         success:function(response){
//             table = $( '#example' ).DataTable();       
//         },
   
//       });
//  } );

//  $(document).ready(function() {
//     $.ajax( {
//         url: "http://dummy.restapiexample.com/api/v1/employees",
//         type='get',
//         columns=[ {
//                 label: "Id:",
//                 name: "id"
//             }, {
//                 label: "Employee Name:",
//                 name: "employee_name"
//             }, {
//                 label: "Employee Salary:",
//                 name: "employee_salary"
//             }, {
//                 label: "Employee Age:",
//                 name: "employee_age",
//             },
//             {
//                 label:"Profile Image",
//                 name:"profile_image"
//             }
//         ],
//         success:function(data){
//             var jsonData=data;

//             $('#example').DataTable({
//                 "data": jsonData,
//                 "columns": [
//                   { "data": "id" },
//                   { "data": "name" },
//                   {"data":"username"},
//                   {"data":"email"}
//                 ]
//             });
//         }
//     } );
// } );

$(document).ready(function(){
    $.ajax({
        url:"http://dummy.restapiexample.com/api/v1/employees",
        type:"get",
        success: function(data){
            var jsonData =JSON.parse(data);
            console.log(jsonData);
            
            $('#example').DataTable({
                "data": jsonData,
                "columns": [
                  { "data": "id" },
                  { "data": "employee_name" },
                  {"data":"employee_salary"},
                  {"data":"employee_age"},
                  {"data":"profile_image"}
                ]
            });
        }
    });
});