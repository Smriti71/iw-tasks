import unittest
import demo

class MyTestCase(unittest.TestCase):
    def test_sub(self):
        self.assertEqual(demo.sub(4,5),-1)
        self.assertEqual(demo.sub(-1,-1),0)
        self.assertEqual(demo.sub(-1,1),-2)
    def test_add(self):
        self.assertEqual(demo.add(4,5),9)
        self.assertEqual(demo.add(-1,-1),-2)
        self.assertEqual(demo.add(-1,1),0)
    def test_mul(self):
        self.assertEqual(demo.mul(4,5),20)
        self.assertEqual(demo.mul(-1,1),-1)
        self.assertEqual(demo.mul(-1,-1),1)
    def test_divide(self):
        self.assertEqual(demo.divide(15,5),3)
        self.assertEqual(demo.divide(-1,1),-1)
        self.assertEqual(demo.divide(-1,-1),1)
        with self.assertRaises(ZeroDivisionError):
            demo.divide(10,0)

if __name__ == '__main__':
    unittest.main()
